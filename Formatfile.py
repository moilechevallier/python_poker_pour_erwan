from tkinter import *
from PIL import ImageTk, Image
from win32api import *
import os
import time

class format_background:
    xscreen = GetSystemMetrics(0)  # largeur
    yscreen = GetSystemMetrics(1)  # hauteur
    fontbase = Image.open("src\\Background\\Backgroundbase1.gif")
    fontbase = fontbase.resize((xscreen, yscreen))
    fontbase.save("src\\Background\\Background1.gif")
    fontbase = Image.open("src\\Background\\Backgroundbase2.gif")
    fontbase = fontbase.resize((xscreen, yscreen))
    fontbase.save("src\\Background\\Background2.gif")

class format_gif_mettaton:
    xscreen = GetSystemMetrics(0)  # largeur
    yscreen = GetSystemMetrics(1)  # hauteur
    xmettaton = int(xscreen/6.97692140958509)
    ymettaton = int(yscreen/6)
    for ind in range(1,74):
        if ind<10:
            file = Image.open("src\\Mettaton\\IMG0000"+str(ind)+".gif")
            file = file.resize((xmettaton, ymettaton))
            file.save("src\\Mettaton\\Mettatonresize\\IMG0000"+str(ind)+".gif")
        else:
            file = Image.open("src\\Mettaton\\IMG000"+str(ind)+".gif")
            file = file.resize((xmettaton, ymettaton))
            file.save("src\\Mettaton\\Mettatonresize\\IMG000" + str(ind) + ".gif")

