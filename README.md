# Projet Poker

##Projet Poker en Python

Ce projet vous propose de jouer à un jeu de poker via une interface graphique créée sur TKinter,le jeu vous donne des cartes
aléatoire et vous confronte à 3 IA.

## Prérequis :
Installer la police d'écriture nécéssaire (qui se trouvera dans le git)

##LANCEMENT :
Le jeu se lance en plein écran

Lancer à partir du Lancement_poker.py (le fichier parle de lui même)

## Les étapes réalisées

 - Génération de cartes aléatoires pour les mains de chaques joueurs et pour la table
 - Création des 3 IA et du joueur 1
 - Génération d'une seed permettant de pouvoir rejouer une partie
 - Interface graphique permettant de jouer au poker

##Lien au repo Git

https://gitlab.com/moilechevallier/python_poker_pour_erwan

## version Tkinter

tk==0.1.0


