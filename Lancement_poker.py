import os
import time
from tkinter import *
import pygame
from win32api import *
import Formatfile
import format_text
import cards
import random
import Remplacecard
def positionclick(event):
    print("clicked at", event.x, event.y)


def key(event):
    print("pressed", event.char)
    cache_seed = open(".\\src\\Cache_python\\seed.txt", "w")
    if event.char == "1" or event.char == "2" or event.char == "3" or event.char == "4" or event.char == "5" or event.char == "6" or event.char == "7" or event.char == "8" or event.char == "9" or event.char == "0":
        cache_seed.write(event.char)
    cache_seed.close()


def callback(event):

    xscreen = GetSystemMetrics(0)  # largeur
    yscreen = GetSystemMetrics(1)  # hauteur

    print("clicked at", xscreen / event.x, yscreen / event.y)
    cache_boutton = open(".\\src\\Cache_python\\boutton.txt", "w")
    if yscreen / event.y < 1.12 and yscreen / event.y > 1.01:

        clickbuttonsound = pygame.mixer.Sound('.\\src\\ost\\Buttonboop.wav')


        if xscreen / event.x < 11.7 and xscreen / event.x > 4.12:
            print("boutton quitté cliqué")
            cache_boutton.write("1")
            clickbuttonsound.play()
        elif xscreen / event.x < 3.2411347517730498 and xscreen / event.x > 2.1624605678233437:
            print("boutton - cliqué")
            cache_boutton.write("2")
            clickbuttonsound.play()
        elif xscreen / event.x < 1.8577235772357723 and xscreen / event.x > 1.4416403785488958:
            print("boutton + cliqué")
            cache_boutton.write("3")
            clickbuttonsound.play()
        elif xscreen / event.x < 1.314477468839885 and xscreen / event.x > 1.0920353982300885:
            print("boutton Suivant cliqué")
            cache_boutton.write("4")
            clickbuttonsound.play()
    if xscreen / event.x > 4.127764127764128 and yscreen / event.y > 23.333333333333332:
        print("boutton seed cliqué")
        cache_boutton.write("5")

    cache_boutton.close()


class start(os):
    cache_carte = open(".\\src\\Cache_python\\cards1.txt", "w")
    cache_carte.write("")
    cache_carte.close()
    seed = str(random.randint(1000,9999))
    statue_partie = True

    while True:
        game = cards.PokerCards()
        game.play()
        cartesgen = ""

        cache_carte = open(".\\src\\Cache_python\\cards1.txt", "r")
        carteseelect = cache_carte.read()
        cartesgen = cartesgen + carteseelect
        cache_carte.close()
        mains1 = cartesgen[0:5]
        bot1 = cartesgen[6:12]
        bot2 = cartesgen[12:18]
        bot3 = cartesgen[18:24]
        table = cartesgen[24:199]
        mains1 = Remplacecard.Replacecard.replacevalue(mains1)
        bot1 = Remplacecard.Replacecard.replacevalue(bot1)
        bot2 = Remplacecard.Replacecard.replacevalue(bot2)
        bot3 = Remplacecard.Replacecard.replacevalue(bot3)
        table = Remplacecard.Replacecard.replacevalue(table)
        cache_carte.close()



        pygame.mixer.init()
        backgroundsound = pygame.mixer.Sound('.\\src\\ost\\background_music.wav')
        backgroundsound.play(loops=-1)
        mise = 0
        cache_boutton = open(".\\src\\Cache_python\\boutton.txt", "w")
        cache_seed = open(".\\src\\Cache_python\\seed.txt", "w")
        cache_boutton.write("0")
        cache_seed.write("X")
        cache_boutton.close()
        cache_seed.close()
        sortie = False
        # variable button explication :
        # = 0 : pas enclenché
        # = 1 : quitter
        # = 2 : -
        # = 3 : +
        # = 4 : suivant
        # = 5 : reroll
        state = 1
        Formatfile.format_background()
        Formatfile.format_gif_mettaton()
        etape_poker = 0
        xscreen = GetSystemMetrics(0)  # largeur
        yscreen = GetSystemMetrics(1)  # hauteur

        root = Tk()
        # met le jeu en fullsceen
        root.attributes('-fullscreen', True)
        # crée le canva avec les paramètrres
        canvas1 = Canvas(root, width=400, height=400)
        canvas1.focus_force()
        canvas1.pack(fill="both", expand=True)
        canvas1.config(background='Black')
        # Display image
        bg = PhotoImage(file="src\\Background\\Background1.gif")
        canvas1.create_image(0, 0, image=bg, anchor="nw")
        # Execute tkinter
        a = 0
        b = 0
        animation_mettaton = 1
        phase_metaton = 0
        compteur_texte = 0
        longueur_texte = 1

        while statue_partie == True:
            cache_text = open(".\\src\\Cache_python\\texte.txt", "r")
            time.sleep(0.009)
            # permet l'animation de l'image du fond
            if (a == 50):
                bg = PhotoImage(file="src\\Background\\Background1.gif")
                canvas1.create_image(0, 0, image=bg, anchor="nw")
            if (a == 0):
                bg = PhotoImage(file="src\\Background\\Background2.gif")
                canvas1.create_image(0, 0, image=bg, anchor="nw")
            # fin de l'annimation de l'image du fond
            if animation_mettaton < 10:
                file = "src\\Mettaton\\Mettatonresize\\IMG0000" + str(int(animation_mettaton)) + ".gif"

            else:
                file = "src\\Mettaton\\Mettatonresize\\IMG000" + str(int(animation_mettaton)) + ".gif"
            photomettaton = PhotoImage(file=file)
            canvas1.create_image(xscreen / 9.863309352517986, yscreen / 1.6766304347826086, anchor='nw', image=photomettaton)

            file = "./src/testclick.gif"
            testclick = PhotoImage(file=file)
            canvas1.create_image(xscreen / 4.1, yscreen / 1.02, anchor='nw', image=testclick)
            canvas1.bind("<Key>", key)
            canvas1.bind("<Button-1>", callback)
            var = StringVar()

            label = Label(canvas1, text=seed, bg='Black', fg='#F8841D', font=('Determination Mono', int(yscreen/70)))
            label.place(x=xscreen / 9.64824120603015, y=xscreen / 200)

            #mise en place du texte
            format_text.format_text_mettaton.outpoutvariabletext(state,longueur_texte)
            cache_text = open(".\\src\\Cache_python\\texte.txt", "r")
            texte_dialogue = cache_text.read()
            label = Label(canvas1, text=texte_dialogue, bg='Black', fg='White', font=('Determination Mono', int(yscreen / 50)))

            cache_text.close()

            #mise en place des cartes
            if (state > 14):
                label_main = Label(canvas1, text=mains1, bg='White', fg='Black', font=('Determination Mono', int(yscreen / 70)))
                label_main.place(x=xscreen / 2.25, y=xscreen / 3)
            if (state > 18):
                label_bot1 = Label(canvas1, text=bot1, bg='White', fg='Black', font=('Determination Mono', int(yscreen / 70)))
                label_bot1.place(x=xscreen / 1.5, y=xscreen / 5)
                label_bot2 = Label(canvas1, text=bot2, bg='White', fg='Black', font=('Determination Mono', int(yscreen / 70)))
                label_bot2.place(x=xscreen / 2.25, y=xscreen / 5)
                label_bot3 = Label(canvas1, text=bot3, bg='White', fg='Black', font=('Determination Mono', int(yscreen / 70)))
                label_bot3.place(x=xscreen / 4, y=xscreen / 5)
            if (state > 16):
                label_table = Label(canvas1, text=table, bg='White', fg='Black', font=('Determination Mono', int(yscreen / 70)))
                label_table.place(x=xscreen / 2.25, y=xscreen / 4)

            canvas1.update()
            a = a + 1
            b = b + 1
            compteur_texte = compteur_texte + 1
            animation_mettaton = animation_mettaton + 0.5

            if a == 100:
                canvas1.delete("all")
                a = 0
            if compteur_texte == 7:
                longueur_texte = longueur_texte + 1
                compteur_texte = 0
            label.place(x=xscreen / 4, y=xscreen / 2.5)

            if animation_mettaton >= 74:
                animation_mettaton = 1
            cache_boutton = open(".\\src\\Cache_python\\boutton.txt", "r")
            # condition bouton (si il a cliqué sur le bouton, il se passe une action)
            boutton = cache_boutton.read()
            if boutton == "1":  # Quitter le programme
                quit()
            elif boutton == "2" and mise > 0 and etape_poker == "mise":  # retire de mise
                mise = mise - 1
            elif boutton == "3" and mise < 10 and etape_poker == "mise":  # ajoute de mise
                mise = mise + 1
            elif boutton == "4" :  # Bouton suivant
                if texte_dialogue[-1] != "_":
                    longueur_texte = 100
                else:
                    state = state+1
                    label = Label(canvas1, text="                                                                ", bg='Black', fg='White',font=('Determination Mono', int(yscreen / 50)))
                    label.place(x=xscreen / 4, y=xscreen / 2.5)
                    longueur_texte = 1
            elif boutton == "5":  # Bouton reroll
                cache_cartes = open(".\\src\\Cache_python\\cards1.txt", "w")
                cache_cartes.write("")
                cache_cartes.close()
                random.seed(seed)
                game = cards.PokerCards()
                game.play()
                cartesgen = ""

                cache_carte = open(".\\src\\Cache_python\\cards1.txt", "r")
                carteseelect = cache_carte.read()
                cartesgen = cartesgen + carteseelect
                cache_carte.close()
                mains1 = cartesgen[0:5]
                bot1 = cartesgen[6:12]
                bot2 = cartesgen[12:18]
                bot3 = cartesgen[18:24]
                table = cartesgen[24:199]
                mains1 = Remplacecard.Replacecard.replacevalue(mains1)
                bot1 = Remplacecard.Replacecard.replacevalue(bot1)
                bot2 = Remplacecard.Replacecard.replacevalue(bot2)
                bot3 = Remplacecard.Replacecard.replacevalue(bot3)
                table = Remplacecard.Replacecard.replacevalue(table)
                cache_carte.close()
            cache_boutton.close()
            cache_seed = open(".\\src\\Cache_python\\seed.txt", "r")
            char_seed = cache_seed.read()
            if char_seed != "X" and (char_seed == "1" or char_seed == "2" or char_seed == "3" or char_seed == "4" or char_seed == "5" or char_seed == "6" or char_seed == "7" or char_seed == "8" or char_seed == "9" or char_seed== "0"):
                seed = seed[1] + seed[2] + seed[3] + char_seed
            cache_seed.close()
            cache_seed = open(".\\src\\Cache_python\\seed.txt", "w")
            cache_boutton = open(".\\src\\Cache_python\\boutton.txt", "w")
            cache_text = open(".\\src\\Cache_python\\texte.txt", "w")
            cache_boutton.write("0")
            cache_seed.write("X")
            cache_text.write("0")
            cache_seed.close()
            cache_boutton.close()
            cache_text.close()
            #if you read this comment, you get Rickrolled
        canvas1.destroy()
        statue_partie = True
            


if __name__ == "__main__":

    start()

