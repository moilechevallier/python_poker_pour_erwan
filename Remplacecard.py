class Replacecard:
    def replacevalue(cards):
        cards = cards.replace('B', '10')
        cards = cards.replace('C', 'V')
        cards = cards.replace('D', 'Q')
        cards = cards.replace('E', 'K')
        cards = cards.replace('F', '♥')
        cards = cards.replace('G', '♠')
        cards = cards.replace('H', '♣')
        cards = cards.replace('I', '♦')
        return cards
