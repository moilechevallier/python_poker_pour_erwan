import string, math, random


class Card(object):
    RANKS = ("2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E")


    SYMBOLS = ('F', 'G', 'H', 'I')





    def __init__(self, rank, symbol):
        self.rank = rank
        self.symbol = symbol

    def __str__(self):
        if self.rank == 14:
            rank = 'A'
        elif self.rank == 13:
            rank = 'R'
        elif self.rank == 12:
            rank = 'D'
        elif self.rank == 11:
            rank = 'V'
        else:
            rank = self.rank
        return str(rank) + self.symbol

    def __eq__(self, other):
        return (self.rank == other.rank)

    def __ne__(self, other):
        return (self.rank != other.rank)

    def __lt__(self, other):
        return (self.rank < other.rank)

    def __le__(self, other):
        return (self.rank <= other.rank)

    def __gt__(self, other):
        return (self.rank > other.rank)

    def __ge__(self, other):
        return (self.rank >= other.rank)


class Deck(object):
    def __init__(self):
        self.deck = []
        for symbol in Card.SYMBOLS:
            for rank in Card.RANKS:
                card = Card(rank, symbol)
                self.deck.append(card)

    def shuffle(self):
        random.shuffle(self.deck)

    def __len__(self):
        return len(self.deck)

    def deal(self):
        if len(self) == 0:
            return None
        else:
            return self.deck.pop(0)


class PokerCards(object):
    def __init__(self):
        self.deck = Deck()
        self.deck.shuffle()
        self.hands = []
        self.table = []
        self.tlist = []
        numCards_in_Hand = 2
        numCards_on_Table = 3
        num_players = 4

        for i in range(num_players):
            hand = []
            for j in range(numCards_in_Hand):
                hand.append(self.deck.deal())
            self.hands.append(hand)

        for k in range(numCards_on_Table):
            self.table.append(self.deck.deal())

    def play(self):
        for i in range(len(self.hands)):
            sortedHand = sorted(self.hands[i], reverse=True)
            hand = ''

            for card in sortedHand:
                hand = hand + str(card) + ' '
            print('Player ' + str(i + 1) + ': ' + hand)
            cache_cartes = open(".\\src\\Cache_python\\cards1.txt", "a")
            cache_cartes.write(hand)
            cache_cartes.close()

        table = ''
        for card in self.table:
            table = table + str(card) + ' '
        print('Table ' + ': ' + table)
        cache_cartes = open(".\\src\\Cache_python\\cards1.txt", "a")
        cache_cartes.write(table)
        cache_cartes.close()



