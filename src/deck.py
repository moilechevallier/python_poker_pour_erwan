
import random
from typing import List

from .card import Card


class CreateDeck:
    def __init__(self, lowest_type: int):
        self._lowest_type = lowest_type

    def create_deck(self):
        return Deck(self._lowest_type)


class Deck:
    def __init__(self, lowest_type: int):
        self._cards: List[Card] = [Card(type, family) for type in range(lowest_type, 15) for family in range(0, 4)]
        self._discard: List[Card] = []
        random.shuffle(self._cards)

    def delete_cards(self, num_cards=1) -> List[Card]:
        new_cards = []
        if len(self._cards) < num_cards:
            new_cards = self._cards
            self._cards = self._discard
            self._discard = []
            random.shuffle(self._cards)
        return new_cards + [self._cards.pop() for _ in range(num_cards - len(new_cards))]

    def push_cards(self, discard: List[Card]):
        self._discard += discard